<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
	<link rel="stylesheet" type="text/css" href="../../public/maincontent/css/style.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.4.1/components/icon.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
</head>
<body>
	<header class="hl-header">
		<h1 class="header-title">
			Vitrolla
		</h1>
		<div class="hl-menu">
			<div>
				<a href="#addplaylist" rel="modal:open">
					Adicionar Música
				</a>
			</div>
		</div>
	</header>

	<main>
		<table class="table-songs">
			<tr>
				<th>Foto</th>
				<th>Música</th>
				<th>Artista/Banda</th>
				<th>Estilo Musical</th>
			</tr>
			<tr>
				<td>Imagem Artista</td>
				<td>Do I Ever</td>
				<td>Chris Brown</td>
				<td>R&B</td>
				<td> <i class="pencil icon"></i></td>
				<td><i class="trash icon"></i></td>
			</tr>
		</table>
	</main>


	<!-- modal content -->
	<div id="addplaylist" class="modal modal-styled">
		<div>
			<input type="text" name="" placeholder="Nome da Música">
		</div>
		<div>
			<input type="text" name="" placeholder="Artista ou Banda">
		</div>
		<div>
			<button>Adicionar</button>
		</div>
	</div>


	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
</body>
</html>